/*
 * Module: pras.example.coursera, Project: SLI
 * Copyright (C) Aviva UK Life, All rights reserved
 */
package pras.example.coursera.dfs


/**
 * KargerRun.groovy - Executes Karger's algo on Graph.
 *
 * @author singhp5
 * @since 8 Aug 2013
 */

def createG() {

    def g = new Graph()
    def f = new File("c:/singhp5/Downloads/kargerMinCut.txt")
    def graphls =[]
    f.readLines().each {
        graphls<< it.split(" ")*.toInteger()
    }
    // def ls = [[1, 2, 3,4,7], [2, 1, 4, 3], [3, 4, 1, 2], [4, 1,2,3,5], [5,4,6,7,8],[6,5,7,8],[7,1,5,6,8],[8,5,6,7]]
    //[ [1,2,5,6],[2,1,3,5,6],[3,2,4,7,8],[4,3,7,8],[5,1,2,6],[6,1,2,5,7],[7,6,8,3,4],[8,7,3,4]]
    g.populateGraph(graphls)
    return g
}

edgeList =[]
def runTime(def a){
    def list = []
    a.times {
        def g = createG()
        while(g.countNodes() > 2) {
            def randomEdge = g.randomEdgeToBeContracted()
            edgeList << randomEdge

            g.contractEdge(randomEdge[0],randomEdge[1])
        }

        list << g.countEdges()
    }

    return list
}
def a = runTime(100)
println a.min()/2
