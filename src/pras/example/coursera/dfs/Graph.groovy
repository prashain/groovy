/*
 * Module: pras.example.coursera, Project: SLI
 * Copyright (C) Aviva UK Life, All rights reserved
 */
package pras.example.coursera.dfs


/**
 * Graph.groovy - Represents Graph in groovy and further computes Min-Cut using Karger's min-Cut algorithm
 *
 * @author singhp5
 * @since 8 Aug 2013
 */
class Graph{

    private List<List<Integer>> adjList = []
    private int nodes

    Graph(int _nodes){
        this.nodes = _nodes
        buildGraph()
    }

    Graph(){
    }

    private buildGraph(){
        (1..nodes).each { adjList << [it]}
    }

    def populateGraph(List<List<Integer>> list){
        adjList = list
    }

    // add Edge between two nodes
    def addEdge(def a, def b){
        adjList.find {
            it.head() == a
        }.add(b)
    }
    def removeEdge(def u, def v){
        def subList = adjList.find{ it.head() == u}
        def iter = subList.iterator()
        while(iter.hasNext()){
            if(iter.next() ==  v){
                iter.remove()
                return
            }
        }
    }
    // answers whether graph contains an edge
    def boolean hasEdge(def u, def v) {
        adjList.find {
            it.head() == u
        }.contains(v)
    }
    def String printGraph(){
        adjList.toString()
    }

    def countEdges(){
        adjList.collect {
            it.tail().size()
        }.sum()
    }
    def listofNodes()
    {
        return adjList.collect { it.head()}

    }

    def listOfEdges(){
        def  lst = []
        adjList.each {
            def x= it.head()
            it.tail().each {
                lst << [x, it]
            }
        }
        return lst
    }
    def countNodes(){
        adjList.collect { it.head() }.size()

    }

    def fuseVToU(def u , def v){
        def nodeU = adjList.find {it.head() ==u}
        def uidx = adjList.indexOf(nodeU)
        adjList.putAt(uidx, nodeU.minus(v))
    }

    def fuseFromAllNodes(def u , def v) {
        adjList.each {
            def listiter = it.listIterator()
            while(listiter.hasNext())
            {
                if(listiter.next()== v)
                {
                    listiter.set(u)
                }
            }
        }
    }

    def removeSelfLoop(v){
        def node = adjList.find { it.head() == v && it.tail().contains(v) }
        def mutatedNode= [node.head()]+ node.tail().minus(v)
        adjList.putAt ( adjList.indexOf(node), mutatedNode)
    }

    def removeNode(def v)
    {

        def iter = adjList.iterator()
        while(iter.hasNext()){
            if(iter.next().head() == v)
            { iter.remove()
                break
            }
        }
        // removeEdgesFromAllNodes(v)

    }

    // contract node u and v
    // v merges to u
    def contractEdge(def u , def v){
        u = u
        // vfuses into u
        v =v
        // add v childs to u
        def t = adjList.find{
            it.head() == v
        }
        def  nodeU = adjList.find{ it.head() == u }
        def uidx = adjList.indexOf(nodeU)
        nodeU.addAll(t)
        adjList.putAt(uidx,nodeU)
        removeNode(v)
        // change all v's to u's
        fuseVToU(u,v)
        // remove self loop
        fuseFromAllNodes(u,v)
        removeSelfLoop(u)

    }
    /*
     *  Random edges to contract
     */
    def randomEdgeToBeContracted()
    {
        def xs = listOfEdges()
        def random = new Random()
        def i = random.nextInt(xs.size())
        return xs[i]
    }

}

