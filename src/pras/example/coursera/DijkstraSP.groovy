/*
 * Module: pras.example.coursera, Project: SLI
 */
package pras.example.coursera


/**
 * DijkstraSP.groovy - TODO singhp5 COMMENT MISSING.
 *
 * @author singhp5
 * @since 7 Aug 2013
 */
class ResultTuple{
    def d
    def p

    ResultTuple(def distance, def predec){
        d = distance
        p = predec
    }
}
class Vertex {
    def value
    def weight

    Vertex(def v, def w){
        value = v
        weight = w
    }
}

class VertexComparator implements Comparator<Vertex> {

    @Override
    public int compare(Vertex o1, Vertex o2) {
        return o1.weight.compareTo(o2.weight);
    }
}

inf = 1000000
def readUndirectedGraph(def filePath){
    def adjList = new HashMap<Integer, HashMap<Integer, Vertex>>()
    def f = new File(filePath)
    adjList = [:]
    f.eachLine{line ->
        data = line.split()
        tmp =[:]
        data.tail().each{
            nodes = it.split(",")*.toInteger()
            tmp.put(nodes[0],nodes[1])
        }
        adjList[data[0].toInteger()] =tmp
    }
    return adjList
}

def relax(def W, def u, def v, def D, def P) {
    D.get(u,inf)
    d= D.get(u,inf) + W[u][v]
    if( d < D.get(v,inf))
        D[v] =d
    P[v] = u
    return true
}

/**
 *        G is undirected graph
 *        s is node for which shortest path need to  be computed
 **/
def dijkstra(def G,def s) {
    D = [:]
    D[s] = 0
    //s:0

    P = [:]
    Q = new PriorityQueue(200,new VertexComparator())
    Q.add(new Vertex(s,0))
    S = new HashSet()
    while(Q.size() >0)
    {
        node = Q.poll()
        u = node.value
        if(S.contains(u)) continue
        else S.add(u)
        for(t in G[u])
        {
            relax(G,u,t.key,D,P)
            // println "u is ${u} and v is ${t.key} and distance is ${D[t.key]}"
            Q.add(new Vertex(t.key,D[t.key]))
        }
    }
    return D.sort()
}

g = readUndirectedGraph("./dijkstraData.txt")

// Nodes to compute shortest path from.
test = [
    7,
    37,
    59,
    82,
    99,
    115,
    133,
    165,
    188,
    197
]
result = [:]
test.each{
    result[it] = dijkstra(g,it)[1]
}

println result