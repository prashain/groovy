package pras.example.coursera


/**
 * TwoSum.groovy - TODO singhp5 COMMENT MISSING.
 *
 * @author singhp5
 * @since 16 Aug 2013
 */
import groovy.time.*

def f = new File("C:/singhp5/Downloads/2Sum.txt")
def lst = new HashSet()
def start = new Date()

f.eachLine {
    lst << it.toBigInteger()
}


def LL = -10000
def UL = 10000
def sortStart = new Date()
xs = lst.toList().sort()
def sortStop = new Date()
println TimeCategory.minus(sortStop,sortStart)

def binarySearch(def xs, def elem){
    ubound = xs.size()-1
    lbound = 0
    while (ubound > lbound) {
        testpos = lbound + ((ubound-lbound).intdiv(2));

        if (xs[testpos] >= elem) {
            ubound = testpos;
        }
        else {
            lbound = testpos + 1;
        }
    }
    return ubound
}
cnt =0
sumList = new HashSet()

def mainP = new Date()
iter = xs.listIterator()
while(iter.hasNext()){
    a = iter.next()
    def ub = UL -a
    def lb = LL -a
    lower = binarySearch(xs,lb)
    upper = binarySearch(xs,ub)

    if(upper > lower){

        (lower..upper-1).each{
            sumList.add(xs[it]+a)
        }
        cnt += upper-lower
    }
    iter.remove()
}
println "Total count is ${cnt}"
println TimeCategory.minus(new Date(),mainP)
def stop = new Date()
def duration = TimeCategory.minus(stop,start)
println "duration is ${duration}"

println sumList.size()
