  import groovy.transform.ToString
 
  
  @ToString(includeNames=true, includeFields=true , excludes='')
  class Vertex {
      def val
      boolean explored
      
      Vertex(def value, boolean flag){
          val = value
          explored = flag
      }

  }
  class Graph{
     private  List<List<Vertex>> list = []
     private List R = []
     
     Graph(def adjList){
         buildGraph(adjList)
     }
     
     private  buildGraph(def adjList){
        list.addAll(adjList.collect {
            it.collect{ a ->
                new Vertex(a,false)
            }
        }   )  
          
     }
     def edgeListOfNode(def node){
         list.find { it.head().val == node}}
         
     public List graphList(){
         return list
     }
     def size(){
         list.size()
     }
     
     def setVertex( def vertex){
        list.each{ val ->
            def listiter = val.listIterator()
            while(listiter.hasNext()){
                    if(listiter.next().val ==vertex){
        
                    listiter.set(new Vertex(vertex,true))
                    }
                }
            }
     }
     
def dfs(def u) {    
       setVertex(u)
       R << u
      // for all edges recursively call dfs
       for(l in edgeListOfNode(u))
       {
           //println l.val
           if(!l.explored){
               dfs(l.val)
           }
       }
       return R
     }     

 

 
}
def f = new File("/Users/prayadav1/Downloads/kargerMinCut.txt")
def graphls = []
         f.readLines().each {
            graphls<< it.split(" ")*.toInteger()
             }
def g = new Graph(graphls)
//println g.graphList()
println g.dfs(4)


