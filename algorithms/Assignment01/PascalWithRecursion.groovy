/**
*
*    Pascal triangle using recursion - Tail call recursion
*               1
*              1 1
*             1 2 1
*            1 3 3 1
*            1 4 6 4 1
*
**/

int pascalTriangle (int col, int row)
{
   if(col ==1 || col == row) return 1
   else{
       return (pascalTriangle(col-1, row-1) + pascalTriangle(col, row-1))
   }
}
// 2 row has 2 elements with 2 elements both of which are 1 1
//3d row as three elements out of which 2nd element = Sum of 1 and 2nd elemnts of previous row
// 4th row has 4 elements out of which 1 and 2th are 1 , 1, and 2nd elemnt = Sum of (1 and 2 of previous row), 3rd is sum of ( 2 and 3)
//Pcr = if c==1 then 1
//if c==r then 1 else Pcr = Pc-1r-1 + Pcr-1

// P11 1
// p12 = 1 P22 =2
// P13 = 1, P33 = 1 P23 = P12 +P11 (2)

assert pascalTriangle(2,3) == 2
assert pascalTriangle(3,3) == 1
assert pascalTriangle(3,4) == 3
