/**
*	Paranthesis balaning using Tail call order recursion.
*
*/

  def balance( List chars)
    {

            def value = loop(0, chars)
            if(value == 0) true
            else false
    }                                             //> balance: (chars: List[Char])Boolean
                def loop(int acc ,def listOfChars ) 
            {
                if(listOfChars.isEmpty() ) acc
                else {
                if(listOfChars.head() == '(' ) { loop(acc+1,listOfChars.tail()) }
                else if(listOfChars.head() == ')' ) { if ( acc == 0) 
                                                        -1
                                                       else
                                                         loop(acc-1,listOfChars.tail()) 
                                                    }
                else {loop(acc,listOfChars.tail()) }
                }
            }
   def str = ")()(".toList()
   
assert balance(str)== false

println balance(str)
 


  




 
