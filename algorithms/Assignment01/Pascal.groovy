/**
*
*    Pascal Triangle implementation
*    Pseudo code: N row Pascal triangle . First two entries would be always 1 and 1 1. 
*            1
*            1    1
*            1    2    1
*            1    3    3    1
*            1    4    6    4    1     
*    A k row pascal triangle will have each row containing elements(0..k). First two rows will always be 1 and 1 1. Third row will have 3 entries , in which first and last entry will be always 1.
*/

import java.util.ArrayList

def pascalTriangle(def n)
{
    def List<List<Integer>> pascalTriangle = new ArrayList<ArrayList<Integer>>();
    for(int i=0; i <n ;i++){
    if(i == 0)
    {
        pascalTriangle << [1]
    }else
    {
         pascalTriangle << fromPreviousRowGetValues(pascalTriangle.get(i-1))
    }
}
 return pascalTriangle   
}

// return computed values for the current row.
def fromPreviousRowGetValues(def list)
{
    def tmpList = new ArrayList<Integer>();
    for(int i=0;i< list.size() ;i++)
    {
      def val = 0
       if (i-1 >= 0)
       {
           val = list.get(i-1)
        }else 
        {
           val = 0
        }
         tmpList << val + list.get(i)
        
    } 
    tmpList<< 1
    
}

def prettyPrint(List list,int indent)
{
    def val = indent
    for(int i = 0; i< list.size(); i++)
    {
    if(list.get(i).class.equals("java.util.ArrayList")){
        prettyPrintNew(list.get(i))
    } else
    {
        println(printTabs(val-i) + list.get(i))
    }     
    }
}

def printTabs(int a)
{
   StringBuilder sb = new StringBuilder()
   a.times 
   {
       sb.append("  ")
   }
   return sb.toString()
}

prettyPrint(pascalTriangle(10),10)