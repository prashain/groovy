/**
*
* CountingChange problem using recursion
*
*/


def countChange(int money, List<Integer> coins)
  {
      return count(coins,coins.size(),money)
  }  

  def count(List<Integer> coins, int numOfCoins, int money )
  {
      if(money == 0)  return 1
      if(money < 0) return 0
      if(numOfCoins <= 0 && money  >= 1)return  0
       return count(coins,numOfCoins-1,money) + count(coins,numOfCoins,money - (coins.get(numOfCoins-1)))
  }        
                                  