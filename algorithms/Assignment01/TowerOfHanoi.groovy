/**
*
*   Rules of the game : Three tower A , B, C
*	You need to move all the disc from Tower A to B (in same order )
*	Rule 1 : YOu cannot place a larger disc to smaller disc
*	 FUNCTION MoveTower(disk, source, dest, spare):
*	IF disk == 0, THEN:
*	    move disk from source to dest
*	ELSE:
*	    MoveTower(disk - 1, source, spare, dest)   // Step 1 above
*	    move disk from source to dest              // Step 2 above
*	    MoveTower(disk - 1, spare, dest, source)   // Step 3 above
*	END IF
*
**/

def towerOfHanoi(int disk, def source , def dest ,def spare)
{
	if(disk == 0)
		return
	else
		{
			towerOfHanoi(disk-1,source, spare, dest)
			println("Moving  disk $disk from $source to $dest")
			move(source, dest,disk)
			towerOfHanoi(disk-1,spare, dest, source)
		}
}

def move(Stack source, Stack dest, def disk)
{
		dest.push(source.pop()) 	
}

def hanoi(int n)
{
	def source = new Stack<Integer>();
	if (n>0)
	{
		(1..n).each {
				source.push it
		}
	}
	def dest =new Stack<Integer>();
	def spare = new Stack<Integer>();
	towerOfHanoi(n, source,dest, spare)	
	println "source is $source and dest is $dest"
}

hanoi(5)
