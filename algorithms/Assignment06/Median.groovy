

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Median {

	public static void main(String[] args) {
		int num = 0;
		long sum = 0;
		boolean insert = false;
		try	{
			BufferedReader br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream("/Users/prayadav1/Downloads/Median.txt"))));
			PriorityQueue<Integer> MinHeap = new PriorityQueue<Integer>(5005);
			PriorityQueue<Integer> MaxHeap = new PriorityQueue<Integer>(5005, comp);
			for(int i=0; i<10000; i++)	{
				num = Integer.parseInt(br.readLine());
				MinHeap.add(num);
				if(insert)	{
					MaxHeap.add(MinHeap.remove());
					sum += MaxHeap.peek();
					insert = false;
				} else	{
					insert = true;
					if(i!=0 && MaxHeap.peek() > MinHeap.peek())	{
						num = MaxHeap.remove();
						MaxHeap.add(MinHeap.remove());
						MinHeap.add(num);
					}
					sum += MinHeap.peek();
				}
			}
			br.close();
		} catch(IOException e)	{
			e.printStackTrace();
		}
		println sum
		System.out.println(sum%10000);
	}
	
	private static final Comparator<Integer> comp = new Comparator<Integer>()	{
		public int compare(Integer a, Integer b)	{
			if(a > b)
				return -1;
			else if(a < b)
				return 1;
			else
				return 0;
		}
	};

}