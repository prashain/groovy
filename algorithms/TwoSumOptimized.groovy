/**
*
*    2Sum problem using sort and binary search.
*    Running time of 90 secs
**/

import groovy.time.*

def f = new File("/Users/prayadav1/Downloads/2sum.txt")
def lst = new HashSet()
def start = new Date()

f.eachLine {
    lst << it.toBigInteger()
}



def LL = -10000
def UL = 10000

def sortStart = new Date()
xs = lst.toList().sort()
//[-7,-5,-2,1,5,7,8,15]
def sortStop = new Date()
println TimeCategory.minus(sortStop,sortStart)


def binarySearch(def xs, def z){
    upperbound = xs.size()-1
    lowerbound = 0
while (upperbound > lowerbound)
{
  testpos = lowerbound + ((upperbound-lowerbound).intdiv(2));

  if (xs[testpos] >= z)
  {
    //  new best-so-far
    upperbound = testpos;
  }
  else
  {
    lowerbound = testpos + 1;
  }
}
return upperbound
}
cnt =0
sumList = new HashSet()

def mainP = new Date()
iter = xs.listIterator()
while(iter.hasNext()){
    a = iter.next()
    def ub = UL -a
    def lb = LL -a 
    lower = binarySearch(xs,lb)
    upper = binarySearch(xs,ub)
    
    if(upper > lower){
    
        (lower..upper-1).each{
            sumList.add(xs[it]+a)
        }
        cnt += upper-lower
    }  
    iter.remove()
}
println "Total count is ${cnt}"
println TimeCategory.minus(new Date(),mainP)
def stop = new Date()
def duration = TimeCategory.minus(stop,start)
println "duration is ${duration}"

println sumList.size()
