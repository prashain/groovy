def list = []
// Bellmans recurring solution
weights = [4,5,8,3]
volumes = [8,10,5,4]
configuration = [:]

matrix=[][]
capacity = 11
ks = 0..capacity


def maxi(def a , def b)
{
    (a>b)? a :b
}


def createMatrix(def k, def j)
{
def list = []
(0..k).each { ks->
   def plist = []
    (0..j).each { js->
     plist.add(O(ks,js))  
    }
    list.add(plist) 
}    
return list
}

def O(k,j)
{
    if(j == 0)
        return 0
    else if (weights[j-1] <= ks[k]  ){
    
    return maxi(O(ks[k],j-1),volumes[j-1] + O(ks[k] - weights[j-1],j-1)  )
    
    }
    else {
    
        return O(ks[k],j-1)
    }
}

def optimalConfiguration(def matrix,def k, def j, def configuration)
{
    if(j ==0) return configuration
    
    if (matrix[k][j] != matrix[k][j-1])
    {
        configuration.put(j,1)
        optimalConfiguration(matrix,k-weights[j-1],j-1,configuration)
     }else
     {
          configuration.put(j,0)
         optimalConfiguration(matrix,k,j-1,configuration)
     }
    return configuration
}
def mat = createMatrix(11,4)
//println mat
println optimalConfiguration(mat,11,4,configuration).sort().values()

