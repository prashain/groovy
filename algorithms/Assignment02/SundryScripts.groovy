import static groovyx.gpars.GParsPool.*
/*
withPool(10){
(10..100).findAllParallel{
    it % (it.toString()*.toInteger().sum()) == 0
}
}


withPool(10){
(1..999).findAllParallel{
    it%3==0 || it %5 ==0 
}.sumParallel()
}

xs = [3,2,1,3,1,2,3]
ps =[]
(0..1000).each {
    if(it==0){
        ps.add(xs[it])
    }
    else if(it >= xs.size() )
    {
        ps << ps[it-1] + xs[it % xs.size()]
    }

    else{
     ps << ps[it-1] + xs[it]
    }
}

println ps.sum()
 [1,2,3].inject { a,b ->
 
 a**3 + b **3
}
*/
[[1,2,3],[3,2,1]].transpose()*.sum()*.intdiv(2) == [1,2,3]

def isMirror (def ps){
    xs = ps.toString()*.toInteger()
    if([xs, xs.reverse()].transpose()*.sum()*.intdiv(2) == xs)
        ps
}

(1..10000).findAll {
    isMirror(it)
}.size()
