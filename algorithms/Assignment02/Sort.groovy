
def mergeSort(def list)
{
    if(list.size <=1) return list
    else
    {
        def mid = list.size().intdiv(2)-1
        def left = list[0..mid]
        def right = list[mid+1 .. list.size()-1]
        return merge(mergeSort(left), mergeSort(right))
    }

}

def merge(def left, def right) {
def result = []
def count=0
while(left.size() >0 && right.size>0)
{
    if(left.head() <= right.head())
    {

       	result.add(left.head())
        left = left.tail()
    }else
    {
        count = count + left.size()
		result.add(right.head())
        right = right.tail()
    }
 }
    
    if(left.size >0)
        result = result + left
    else
        result = result +right
        
println count
return result

}

println (mergeSort([1,3,5,2,4,6]))

// Quicksort implementation

xs = [3,5,7,1,4,2,-19]
 
def file = new File("/Users/prayadav1/Downloads/QuickSort.txt")
def inverted =  file.readLines().collect {
    Integer.parseInt(it)
}

def pivot(def xs)
{
   ( [] << xs.head() << xs.last() << xs.getAt( xs .size().intdiv(2)-1)).sort().getAt(1)
}



def quicksort(def xs){
        if(xs.isEmpty()) return xs
    pivot = pivot(xs)
   def  hi = xs.findAll {
        it > pivot
    }
   def  lo = xs.findAll{ it < pivot }
    def equalItems = xs.findAll{it == pivot}

  quicksort(lo) + equalItems + quicksort(hi)
}
 //quicksort(inverted)

quicksort(xs)
// sort(xs)