// Using randomized selection algorithm to find ith order statistic

import java.util.Random

import groovy.transform.Field
@Field random = new Random();
@Field xs= [3,8,2,5,1,4,7,6]

cp = 0
 
 def swap(def xs, def x , def y)
{
    tmp = xs[x]
    xs[x] =xs[y]
    xs[y]= tmp

}

def pivot(def xs)
{
    def i = random.nextInt(xs.size()-1)
   return xs[i] 
}
def partition(def xs, def p, def r,def l){
    
    def i = p +1
    swap(xs,p,xs.indexOf(l))
    def pivot = xs[p]
    for (int j = p+1;j <= r;j++)
    {
        
        if(xs[j] < pivot)
        {
            swap(xs,i,j)
        
            i++
        }
    }
    swap(xs,p,i-1)
    println "partitioned is :  ${xs} with pivot as ${pivot}"
    return i-1
    
}
 def rSelect(def xs, def p, def r , def i){
     if(p==r) return xs[p]
     q =  partition(xs,0, xs.size()-1,pivot(xs))
     k = q-p+1
     if(i ==k) return xs[q]
     if(i< k) return rSelect(xs,p,q-1,i)
     if(i > k ) return rSelect(xs,q+1,r, i-k)
         

}

def ith = 4
println "${ith} order statistic for ${xs} is  ${rSelect(xs,0,xs.size()-1,ith)}"

