def file = new File("/Users/prayadav1/Downloads/QuickSort.txt")
def inverted =  file.readLines().collect {
    Integer.parseInt(it)
}

def pivot(def xs)
{
      if(xs.size() %2 ==0)
      {
       ( [] << xs.head() << xs.last() << xs.getAt( xs .size().intdiv(2)-1)).sort().getAt(1)   
      }else {
          ( [] << xs.head() << xs.last() << xs.getAt( xs .size().intdiv(2))).sort().getAt(1)
      }
   
}

cp = 0

class Holder {
    def pivot
    def hi
    def lo
}
def swap(def xs, def x , def y)
{
    tmp = xs[x]
    xs[x] =xs[y]
    xs[y]= tmp

}
def partition(def xs,def l , def r)
{
    def i = l+1
      swap(xs,l,xs.indexOf(pivot(xs)))
   // swap(xs,l,r)
    def p = xs[l]
    cp += (r-l)
    for(j=l+1; j<=r;j++)
    {
        if(xs[j] < p)
        {
           
            swap(xs,j,i)
            i+=1
        }
    }
    swap(xs, l,i-1)
    def holder = new Holder()
    holder.with {
        pivot = p
        //lo = xs.findAll{it <p}
        lo =(i>=2)?xs[0..i-2]:[]
        // =xs.findAll{ it > p }
        hi =(xs.isEmpty() || ( i <= xs.size()-1)) ? xs[i..xs.size()-1]:[]
       // println "hi is ${hi} and low is ${lo}"
       // println "hi is ${xs.findAll{it > p}} and low is ${xs.findAll{it < p}}"
        

    }
   //println xs
    return holder
}

 def quicksort(def xs) {
    if(xs.isEmpty()) return xs
     Holder h = partition(xs,0,xs.size()-1)
     quicksort(h.lo) + h.pivot + quicksort(h.hi)
 }
 def xs= [2,8,7,1,3,5,6,4]
// println inverted
a = quicksort(inverted)
println a.size()
 println "Comparisons are ${cp}"